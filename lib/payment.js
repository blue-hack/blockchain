/**
 * Verify payer made transaction.
 * @param {sapna.verilock.PayBill} tx The transaction instance.
 * @transaction
 */
function paymentTransaction(tx) {

    var payer = tx.creditCard.payer

    if (tx.creditCard.threshold < tx.bill.amount) {
        return false
    }

    var confirmedByPayer = codeConfirmed(payer.phoneNum)

    if (confirmedByPayer) {

        if (bankConfirmed(payer.trn)) {

            tx.bill.payed = true

            return getAssetRegistry('sapna.verilock.Bill')
                .then(function(registry) {

                    // Update the asset in the asset registry.
                    return registry.update(tx.bill);

                })
                .then(function() {

                    // Emit an event to notify credit card company payment status of payment.
                    var event = getFactory().newEvent('sapna.verilock', 'Payment');
                    event.creditCompany = tx.creditCompany;
                    event.status = tx.bill.payed ? 'APPROVED' : 'DENIED';
                    emit(event);

                });
        }
    }

}

// Dummy function to simulate the payer confirming the 2FA code sent to
// verify he initiated the payment
function codeConfirmed(phoneNum) {

    return (Math.random() < 0.2 ? 0 : 1)
}

// Dummy function to simulate Veri-Lock requesting the bank to read the blockchain
// to verify the transaction
function bankConfirmed(phoneNum) {

    return (Math.random() < 0.2 ? 0 : 1)
}