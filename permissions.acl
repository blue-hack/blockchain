/**
 * Sample access control list.
 */

rule PayerRule {
    description: "Allow payer to only view their transactions"
    participant(c): "sapna.verilock.Payer"
    operation: READ
    resource(r): "sapna.verilock.CreditCard"
    condition: (r.payer.getIdentifier() == c.getIdentifier())
    action: ALLOW
}

rule BankRule {
    description: "Allow bank to only view payer's transaction"
    participant(c): "sapna.verilock.Bank"
    operation: READ
    resource(r): "sapna.verilock.Bill"
    condition: (r.bank.getIdentifier() == c.getIdentifier())
    action: ALLOW
}

rule NetworkAdminUser {
    description: "Grant business network administrators full access to user resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "**"
    action: ALLOW
}

rule NetworkAdminSystem {
    description: "Grant business network administrators full access to system resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "org.hyperledger.composer.system.**"
    action: ALLOW
}